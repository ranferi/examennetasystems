package com.ranferi.android.examenandroidneta.ui.activities;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.google.firebase.firestore.FirebaseFirestore;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresFragment;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.ui.base.BaseActivity;
import com.ranferi.android.examenandroidneta.util.Util;

import java.util.HashMap;
import java.util.Map;

public class AgregarColaboradorActivity extends BaseActivity {
    private EditText mNameEditText, mEmailEditText, mLatEditText, mLongEditText;
    private Button mAgregarButton;
    private ProgressBar mProgressBar;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_colaborador);

        mProgressBar = findViewById(R.id.progress_bar_agregar);
        setProgressBar(mProgressBar);
        mNameEditText = findViewById(R.id.edit_text_name);
        mEmailEditText = findViewById(R.id.edit_text_email);
        mLatEditText = findViewById(R.id.edit_text_lat);
        mLongEditText = findViewById(R.id.edit_text_long);
        firestore = FirebaseFirestore.getInstance();

        Map<String, Object> dato = new HashMap<>();
        Map<String, String> loc = new HashMap<>();
        loc = Util.newLocation();
        mLatEditText.setText(loc.get("lat"));
        mLongEditText.setText(loc.get("log"));

        mAgregarButton = findViewById(R.id.button_agregar);
        Map<String, String> finalLoc = loc;
        mAgregarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBar();
                int id = Util.newID();
                dato.put("id", id);
                dato.put("location", finalLoc);
                dato.put("mail", mEmailEditText.getText().toString());
                dato.put("name", mNameEditText.getText().toString());

                Log.v(ColaboradoresFragment.TAG, "name: " + mNameEditText.getText().toString()
                        + " mail: " + mEmailEditText.getText().toString() + "id: " + id);

                ColaboradorDB db = new ColaboradorDB(firestore);
                Log.v(ColaboradoresFragment.TAG, "onClick");
                db.addDocument("colaboradores", dato, AgregarColaboradorActivity.this, new ColaboradorDB.OnResultCallback() {
                    @Override
                    public void onResult() {
                        hideProgressBar();
                    }
                });
            }
        });
    }
}