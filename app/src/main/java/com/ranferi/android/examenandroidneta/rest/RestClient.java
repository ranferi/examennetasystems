package com.ranferi.android.examenandroidneta.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ranferi.android.examenandroidneta.rest.service.ColaboradorService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static final String BASE_URL = "https://dl.dropboxusercontent.com/";
    private ColaboradorService mColaboradorService;

    public RestClient() {
        Gson gson = new GsonBuilder().create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        mColaboradorService = retrofit.create(ColaboradorService.class);
    }

    public ColaboradorService getColaboradorService() { return mColaboradorService; }
}