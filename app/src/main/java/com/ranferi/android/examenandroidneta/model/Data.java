package com.ranferi.android.examenandroidneta.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("employees")
    @Expose
    private List<Colaborador> employees = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param employees
     */
    public Data(List<Colaborador> employees) {
        super();
        this.employees = employees;
    }

    public List<Colaborador> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Colaborador> employees) {
        this.employees = employees;
    }

}