package com.ranferi.android.examenandroidneta.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ranferi.android.examenandroidneta.ui.activities.ColaboradorPagerActivity;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.model.Colaborador;

import java.util.List;

public class ColaboradorAdapter extends RecyclerView.Adapter<ColaboradorAdapter.ColaboradorHolder> {

    private List<Colaborador> mColaboradores;
    private Context mContext;

    public ColaboradorAdapter(List<Colaborador> colaboradores, Context context) {
        mColaboradores = colaboradores;
        mContext = context;
    }

    @NonNull
    @Override
    public ColaboradorAdapter.ColaboradorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        return new ColaboradorAdapter.ColaboradorHolder(layoutInflater, parent, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull ColaboradorAdapter.ColaboradorHolder holder, int position) {
        Colaborador colaborador = mColaboradores.get(position);
        holder.bind(colaborador);
    }

    @Override
    public int getItemCount() {
        return mColaboradores.size();
    }

    public static class ColaboradorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mNameTextView;
        private TextView mEmailTextView;
        private Colaborador mColaborador;
        private Context mContext;

        public ColaboradorHolder(LayoutInflater inflater, ViewGroup parent, Context context) {
            super(inflater.inflate(R.layout.list_colaborador_item, parent, false));
            mContext = context;
            itemView.setOnClickListener(this);

            mNameTextView = itemView.findViewById(R.id.colaborador_name);
            mEmailTextView = itemView.findViewById(R.id.colaborador_email);
        }

        public void bind(Colaborador colaborador) {
            mColaborador = colaborador;
            mNameTextView.setText(mColaborador.getName());
            mEmailTextView.setText(mColaborador.getMail());
        }

        @Override
        public void onClick(View view) {
            Intent intent = ColaboradorPagerActivity.newIntent(mContext, mColaborador.getId());
            mContext.startActivity(intent);
        }
    }
}
