package com.ranferi.android.examenandroidneta.util;

import android.content.Context;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresFragment;
import com.ranferi.android.examenandroidneta.model.Colaborador;
import okhttp3.ResponseBody;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Util {

    public static boolean writeResponseBodyToDisk(Context context, ResponseBody body) {
        try {
            String path = Objects.requireNonNull(context).getExternalFilesDir(null) + File.separator;
            File zip = new File(path + "employees_data.json.zip");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(zip);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(ColaboradoresFragment.TAG, "Se ha descargado: " + fileSizeDownloaded + " de " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean checkFileExist(Context context, String fileName) {
        String path = Objects.requireNonNull(context).getExternalFilesDir(null) + File.separator;
        File file = new File(path, fileName);
        return file.exists();
    }


    public static boolean unpackZipFile(Context context, String zipName) {
        String path = Objects.requireNonNull(context).getExternalFilesDir(null) + File.separator;
        InputStream inputStream;
        ZipInputStream zipInputStream;
        try  {
            String fileName;
            inputStream = new FileInputStream(path + zipName);
            zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
            ZipEntry zipEntry;
            byte[] fileReader = new byte[1024];
            int count;

            while ((zipEntry = zipInputStream.getNextEntry()) != null)
            {
                fileName = zipEntry.getName();

                if (zipEntry.isDirectory()) {
                    File file = new File(path + fileName);
                    file.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(path + fileName);

                while ((count = zipInputStream.read(fileReader)) != -1) {
                    fout.write(fileReader, 0, count);
                }

                fout.close();
                zipInputStream.closeEntry();
            }

            zipInputStream.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
    public static String readJsonFile(Context context, String fileName) throws IOException {
        String path = Objects.requireNonNull(context).getExternalFilesDir(null) + File.separator;
        return new String(Files.readAllBytes(Paths.get(path + fileName)));

    }

    public static ArrayList<HashMap<String, Object>> objectToMap(List<Colaborador> colaboradorsList) {
        ArrayList<HashMap<String, Object>> mapList = new ArrayList<HashMap<String, Object>>();

        for (Colaborador temp : colaboradorsList ) {
            ObjectMapper objectMapper = new ObjectMapper();
            HashMap<String, Object> map = objectMapper.convertValue(temp, HashMap.class);
            Log.v(ColaboradoresFragment.TAG, "Lat: " + map.get("location"));
            mapList.add(map);
        }

        return mapList;
    }

    public static int newID() {
        return ThreadLocalRandom.current().nextInt(1, 1001);
    }

    public static HashMap<String, String> newLocation() {
        HashMap<String, String> loc = new HashMap<>();
        Random r = new Random();
        double minLat = 19.25;
        double maxLat = 19.35;
        double minLog = 99.15;
        double maxLog = 99.25;
        double randomLat = ThreadLocalRandom.current().nextDouble(minLat, maxLat);
        double randomLog = ThreadLocalRandom.current().nextDouble(minLog, maxLog);
        Log.v(ColaboradoresFragment.TAG, "Lat: " + randomLat + " Long: " + randomLog);
        // loc.put("lat", "19." + new)
        loc.put("lat", String.valueOf(randomLat));
        loc.put("log", String.valueOf(-randomLog));
        return loc;
    }
}
