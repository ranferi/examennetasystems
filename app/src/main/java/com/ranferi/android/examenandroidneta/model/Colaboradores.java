package com.ranferi.android.examenandroidneta.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Colaboradores {

    @SerializedName("data")
    @Expose
    private Data data;

    /**
     * No args constructor for use in serialization
     *
     */
    public Colaboradores() {
    }

    /**
     *
     * @param data
     */
    public Colaboradores(Data data) {
        super();
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
