package com.ranferi.android.examenandroidneta.db;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresFragment;
import com.ranferi.android.examenandroidneta.model.Colaborador;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ColaboradorDB {
    private FirebaseFirestore db;
    private int mId;
    private boolean mEmpty = false;


    public ColaboradorDB(FirebaseFirestore db) {
        this.db = db;
    }

    public void addDocuments(String collection, List<HashMap<String, Object>> documents) {
        for (HashMap<String, Object> temp : documents) {
            addDocument(collection, temp, null, () -> {

            });
        }
    }

    public void addDocument(String collection, Map document, Context context, OnResultCallback onResultCallback) {
        db.collection(collection)
                .add(document)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        if (context != null) {
                            onResultCallback.onResult();
                            Toast.makeText(context, "Se ha agregado un colaborador", Toast.LENGTH_SHORT).show();
                        }
                        Log.v(ColaboradoresFragment.TAG, "DocumentSnapshot ID: " + documentReference.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.v(ColaboradoresFragment.TAG, "Error al agregar documento", e);
                    }
                });
    }
    public void getAllDocs(String collection, AllDocsCallback allDocsCallback) {
        db.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Colaborador> list = new ArrayList<Colaborador>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.v(ColaboradoresFragment.TAG, "ColaboradorDB.getAllDocs");
                                Colaborador colaborador = document.toObject(Colaborador.class);
                                list.add(colaborador);
                            }
                            allDocsCallback.allDocsResult(list);
                        }
                    }
                });

    }

    public void getDocument(String collection, int id, OnGetDocCallBack onGetDocCallBack) {
        db.collection(collection)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.v(ColaboradoresFragment.TAG, document.getId() + " => " + document.getData());
                                Colaborador colaborador = document.toObject(Colaborador.class);
                                onGetDocCallBack.onGetDocCallback(colaborador);
                            }
                        } else {
                            Log.v(ColaboradoresFragment.TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    public interface OnGetDocCallBack {
        void onGetDocCallback(Colaborador colaborador);
    }

    public interface OnResultCallback {
        void onResult();
    }

    public void checkIDColaborador(String collection, int id, IsEmptySearchCallback isEmptySearchCallback ) {
        db.collection(collection)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            boolean isEmpty = task.getResult().isEmpty();
                            isEmptySearchCallback.isEmptySearch(isEmpty);
                        }
                    }
                });
    }

    public void checkCollectionExists(String collection, CollectionExistsCallback collectionExistsCallback) {
        db.collection(collection)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                        Log.v(ColaboradoresFragment.TAG, "ColaboradorDB.checkCollectionExists");
                        if (task.isSuccessful()) {
                            if (task.getResult().size() > 0)
                                collectionExistsCallback.collectionExists(true);
                            else
                                collectionExistsCallback.collectionExists(false);
                        }
                    }
                });
    }



    public interface IsEmptySearchCallback {
        void isEmptySearch(boolean isEmpty);
    }

    public interface CollectionExistsCallback {
        void collectionExists(boolean exists);
    }

    public interface AllDocsCallback {
        void allDocsResult(List<Colaborador> colaboradorList);
    }
}
