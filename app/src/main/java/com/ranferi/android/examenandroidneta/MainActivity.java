package com.ranferi.android.examenandroidneta;

import androidx.fragment.app.Fragment;

import com.ranferi.android.examenandroidneta.ui.base.SingleFragmentActivity;
import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresFragment;

public class MainActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return ColaboradoresFragment.newInstance();
    }
}