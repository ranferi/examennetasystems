package com.ranferi.android.examenandroidneta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ranferi.android.examenandroidneta.ui.base.BaseActivity;

public class LoginActivity extends BaseActivity {

    private EditText mLoginEmail, mLoginPassword;
    private Button mLoginButton, mRegisterButton;

    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mProgressBar = findViewById(R.id.progress_bar_login);
        setProgressBar(mProgressBar);
        mLoginEmail = findViewById(R.id.login_email);
        mLoginPassword = findViewById(R.id.login_password);
        mLoginButton = findViewById(R.id.login_login_button);
        mRegisterButton = findViewById(R.id.login_register_button);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
    }

    private void login() {
        if (!validateForm(mLoginEmail, mLoginEmail)) return;
        showProgressBar();

        mFirebaseAuth.signInWithEmailAndPassword(mLoginEmail.getText().toString(), mLoginPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            hideProgressBar();
                        } else {
                            hideProgressBar();
                            Toast.makeText(LoginActivity.this, "Fallo la autentificación.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
