package com.ranferi.android.examenandroidneta.ui.base;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresFragment;

public class BaseActivity extends AppCompatActivity {
    public ProgressBar mProgressBar;

    public void setProgressBar(ProgressBar progressBar) {
        mProgressBar = progressBar;
    }

    public void showProgressBar() {
        if (mProgressBar != null) {
            Log.v(ColaboradoresFragment.TAG, "showProgress");
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if (mProgressBar != null) {
            Log.v(ColaboradoresFragment.TAG, "hideProgress");
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    protected boolean validateForm(TextView emailTextView, TextView passwordTextView) {
        boolean valid = true;

        String email = emailTextView.getText().toString();
        String password = passwordTextView.getText().toString();

        if (TextUtils.isEmpty(email)){
            emailTextView.setError("Requerido");
            Toast.makeText(getApplicationContext(),"Por favor, llena los campos",Toast.LENGTH_SHORT).show();
            valid = false;
        } else {
            emailTextView.setError(null);
        }

        if (TextUtils.isEmpty(password)){
            passwordTextView.setError("Requerido");
            Toast.makeText(getApplicationContext(),"Por favor, llena los campos",Toast.LENGTH_SHORT).show();
            valid = false;
        } else {
            passwordTextView.setError(null);
        }

        if (password.length() < 6){
            passwordTextView.setError("Más de 6 caracteres");
            Toast.makeText(getApplicationContext(),"Tu password debe contener al menos 6 caracteres",Toast.LENGTH_SHORT).show();
            valid = false;
        } else {
            passwordTextView.setError(null);
        }

        return valid;
    }


    @Override
    public void onStop() {
        super.onStop();
        hideProgressBar();
    }
}
