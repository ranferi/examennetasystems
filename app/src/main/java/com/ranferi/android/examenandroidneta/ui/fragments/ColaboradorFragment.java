package com.ranferi.android.examenandroidneta.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.model.Colaborador;
import com.ranferi.android.examenandroidneta.model.Location;

public class ColaboradorFragment extends Fragment {
    private static final String ARG_COLABORADOR_ID = "colaborador_id";

    public static ColaboradorFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_COLABORADOR_ID, id);

        ColaboradorFragment fragment = new ColaboradorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private TextView mNameTextView, mEmailTextView, mLatTextView, mLongTextView;
    private MapView mMapView;
    private Colaborador mColaborador;
    private ColaboradorDB mDb;
    private int id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = (int) getArguments().getSerializable(ARG_COLABORADOR_ID);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colaborador, container, false);
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        mDb = new ColaboradorDB(firestore);

        mNameTextView = view.findViewById(R.id.text_name);
        mEmailTextView = view.findViewById(R.id.text_email);
        mLatTextView = view.findViewById(R.id.text_loc_lat);
        mLongTextView = view.findViewById(R.id.text_loc_long);

        mMapView = view.findViewById(R.id.map_colaborador);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView.onCreate(savedInstanceState);
        getColaborador();
    }

    private void getColaborador() {
        mDb.getDocument("colaboradores", id, new ColaboradorDB.OnGetDocCallBack() {
            @Override
            public void onGetDocCallback(Colaborador colaborador) {
                mColaborador = colaborador;
                updateUI(mColaborador);
            }
        });
    }

    private void updateUI(Colaborador colaborador) {
        Location loc = colaborador.getLocation();
        String lat = loc.getLat();
        String log = loc.getLog();

        mNameTextView.setText(colaborador.getName());
        mEmailTextView.setText(colaborador.getMail());
        mLatTextView.setText("Lat: "+ lat);
        mLongTextView.setText("Long: "+ log);

        mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(log));
                googleMap.addMarker(new MarkerOptions().position(latLng).title("Colaborador"));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
