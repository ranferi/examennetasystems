package com.ranferi.android.examenandroidneta.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class ColaboradorLab {

    private List<Colaborador> mColaboradores;

    private static ColaboradorLab sColaboradorLab;

    public static ColaboradorLab get(Context context) {
        if (sColaboradorLab == null) {
            sColaboradorLab = new ColaboradorLab(context);
        }

        return sColaboradorLab;
    }

    private ColaboradorLab(Context context) {
        mColaboradores = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            Colaborador colaborador = new Colaborador();
            colaborador.setName("colaborador #" + i);
            colaborador.setMail("colaborador_" + i + "@gmail.com");
            mColaboradores.add(colaborador);
        }
    }

    public List<Colaborador> getColaboradores() {
        return mColaboradores;
    }

    public Colaborador getColaborador(Integer id) {
        for (Colaborador colaborador : mColaboradores) {
            if (colaborador.getId().equals(id)) {
                return colaborador;
            }
        }
        return null;
    }
}
