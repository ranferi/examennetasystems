package com.ranferi.android.examenandroidneta.app;

import androidx.multidex.MultiDexApplication;

import com.ranferi.android.examenandroidneta.rest.RestClient;

public class App extends MultiDexApplication {
    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();
        restClient = new RestClient();
    }

    public static RestClient getRestClient() {
        return restClient;
    }
}