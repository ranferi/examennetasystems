package com.ranferi.android.examenandroidneta.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.model.Colaborador;
import com.ranferi.android.examenandroidneta.model.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ColaboradoresMapActivity extends AppCompatActivity {
    private TextView mNameTextView, mEmailTextView, mLatTextView, mLongTextView;
    private MapView mMapView;
    private List<Colaborador> mColaboradorList;
    private ColaboradorDB mDb;
    private HashMap<Integer, Marker> mMarkerMap;
    private HashMap<Integer, Colaborador> mColaboradoresMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_colaborador);

        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        mDb = new ColaboradorDB(firestore);

        mNameTextView = findViewById(R.id.text_name);
        mEmailTextView = findViewById(R.id.text_email);
        mLatTextView = findViewById(R.id.text_loc_lat);
        mLongTextView = findViewById(R.id.text_loc_long);
        mMapView = findViewById(R.id.map_colaborador);
        mMapView.onCreate(savedInstanceState);

        getColaboradores();
    }

    private void getColaboradores() {
        mDb.getAllDocs("colaboradores", new ColaboradorDB.AllDocsCallback() {
            @Override
            public void allDocsResult(List<Colaborador> colaboradorList) {
                mColaboradorList = colaboradorList;
                updateUI(mColaboradorList);
            }
        });
    }

    private void updateUI(List<Colaborador> colaboradorList) {
        Location loc = colaboradorList.get(0).getLocation();
        String lat = loc.getLat();
        String log = loc.getLog();

        mNameTextView.setText(colaboradorList.get(0).getName());
        mEmailTextView.setText(colaboradorList.get(0).getMail());
        mLatTextView.setText("Lat: " + lat);
        mLongTextView.setText("Long: " + log);

        mMapView.onResume();
        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMarkerMap = new HashMap<>();
        mColaboradoresMap = new HashMap<>();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLngBounds NETASYSTEMS = new LatLngBounds(
                        new LatLng(19.24, -99.26), new LatLng(19.36, -99.14));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(NETASYSTEMS, 0));
                googleMap.getUiSettings().setTiltGesturesEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                for (Colaborador obj : colaboradorList) {
                    if (!mMarkerMap.containsKey(obj.getId())) {
                        mMarkerMap.put(obj.getId(), createMarker(googleMap, obj.getLocation()));
                        mColaboradoresMap.put(obj.getId(), obj);
                    }
                }
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        Set<Map.Entry<Integer, Marker>> set = mMarkerMap.entrySet();
                        for (Map.Entry<Integer, Marker> integerMarkerEntry : set) {
                            if (integerMarkerEntry.getValue().equals(marker)) {
                                int id = integerMarkerEntry.getKey();
                                Colaborador colaborador = mColaboradoresMap.get(id);
                                if (colaborador != null) updateTextViews(colaborador);
                            }
                        }
                        return false;
                    }
                });
            }
        });
    }

    private void updateTextViews(Colaborador colaborador) {
        Location loc = colaborador.getLocation();
        String lat = loc.getLat();
        String log = loc.getLog();

        mNameTextView.setText(colaborador.getName());
        mEmailTextView.setText(colaborador.getMail());
        mLatTextView.setText("Lat: " + lat);
        mLongTextView.setText("Long: " + log);

    }

    protected Marker createMarker(GoogleMap googleMap, Location loc) {
        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(loc.getLat()), Double.parseDouble(loc.getLog())))
                .anchor(0.5f, 0.5f));
    }

}