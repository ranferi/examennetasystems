package com.ranferi.android.examenandroidneta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ranferi.android.examenandroidneta.ui.base.BaseActivity;

public class RegisterActivity extends BaseActivity {
    private EditText mEmail, mPassword;
    private Button mRegisterButton, mLoginButton;
    private FirebaseAuth firebaseAuth;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mEmail = findViewById(R.id.register_email);
        mPassword = findViewById(R.id.register_password);
        mRegisterButton = findViewById(R.id.register_register_button);
        mLoginButton = findViewById(R.id.register_login_button);

        firebaseAuth = FirebaseAuth.getInstance();

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateForm(mEmail, mPassword)) return;
                showProgressBar();
                firebaseAuth.createUserWithEmailAndPassword(
                        mEmail.getText().toString(),
                        mPassword.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    hideProgressBar();
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    Toast.makeText(getApplicationContext(), "Ya estás registrado", Toast.LENGTH_SHORT)
                                            .show();
                                    finish();
                                } else {
                                    hideProgressBar();
                                    Toast.makeText(getApplicationContext(),
                                            "El email o el password son incorrectos, intenta de nuevo",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        if (firebaseAuth.getCurrentUser() != null){
            hideProgressBar();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            Toast.makeText(getApplicationContext(),
                    "Ya estás loggeado",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

}
