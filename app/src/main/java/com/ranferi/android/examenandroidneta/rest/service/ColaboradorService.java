package com.ranferi.android.examenandroidneta.rest.service;

import com.ranferi.android.examenandroidneta.rest.model.ColaboradorResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ColaboradorService {

    @GET("/s/5u21281sca8gj94/getFile.json")
    Call<ColaboradorResponse> getColaboradorResponse();

    @GET
    Call<ResponseBody> donwloadFileWithURL(@Url String fileUrl);
}
