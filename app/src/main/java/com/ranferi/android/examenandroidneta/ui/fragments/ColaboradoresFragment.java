package com.ranferi.android.examenandroidneta.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.ui.activities.ColaboradoresListActivity;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.app.App;
import com.ranferi.android.examenandroidneta.model.Colaborador;
import com.ranferi.android.examenandroidneta.model.Colaboradores;
import com.ranferi.android.examenandroidneta.rest.model.ColaboradorResponse;
import com.ranferi.android.examenandroidneta.rest.service.ColaboradorService;
import com.ranferi.android.examenandroidneta.ui.activities.AgregarColaboradorActivity;
import com.ranferi.android.examenandroidneta.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ColaboradoresFragment extends Fragment {
    public static final String TAG = ColaboradoresFragment.class.getSimpleName();

    private Button mMisColaboradores, mAgregarColaborador;
    private Colaboradores mColaboradores;
    private ColaboradorService colaboradorService;
    private ColaboradorDB mColaboradorDB;
    private boolean collectionExists = false;

    public static ColaboradoresFragment newInstance() {
        return new ColaboradoresFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colaboradores, container, false);

        colaboradorService = App.getRestClient().getColaboradorService();
        
        mMisColaboradores = view.findViewById(R.id.mis_colaboradores);
        mAgregarColaborador = view.findViewById(R.id.agregar_colaboradores);
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        mColaboradorDB = new ColaboradorDB(firestore);
        
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        mMisColaboradores.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), ColaboradoresListActivity.class);
            startActivity(intent);
        });

        mAgregarColaborador.setOnClickListener(view2 -> {
            Intent intent = new Intent(getActivity(), AgregarColaboradorActivity.class);
            startActivity(intent);
        });

        boolean zipExists = Util.checkFileExist(getActivity(), "employees_data.json.zip");
        boolean jsonExists = Util.checkFileExist(getActivity(), "employees_data.json");

        List<Colaborador> colaboradorList = null;
        List<HashMap<String, Object>> listDocuments = null;

        if (!zipExists)
            fetchColaboradoresResponse();
        if (jsonExists) {
            Log.v(ColaboradoresFragment.TAG, "ColaboradoresFragment");
            mColaboradores = getColaboradoresFromJson();
        } else {
            Util.unpackZipFile(getActivity(), "employees_data.json.zip");
            Log.v(ColaboradoresFragment.TAG, "ColaboradoresFragment.json.failed");
            mColaboradores = getColaboradoresFromJson();
        }
        if (mColaboradores != null)
            colaboradorList = mColaboradores.getData().getEmployees();
        if (colaboradorList != null)
            listDocuments = Util.objectToMap(colaboradorList);
        if (listDocuments != null) {
            List<HashMap<String, Object>> finalListDocuments = listDocuments;
            mColaboradorDB.checkCollectionExists("colaboradores", new ColaboradorDB.CollectionExistsCallback() {
                @Override
                public void collectionExists(boolean exists) {
                    collectionExists = exists;
                    if (!collectionExists)
                        mColaboradorDB.addDocuments("colaboradores", finalListDocuments);
                }
            });
        }
    }

    private void fetchColaboradoresResponse() {
        Call<ColaboradorResponse> call = colaboradorService.getColaboradorResponse();
        call.enqueue(new Callback<ColaboradorResponse>() {
            @Override
            public void onResponse(@NotNull Call<ColaboradorResponse> call, @NotNull Response<ColaboradorResponse> response) {
                if (response.isSuccessful()) {
                    ColaboradorResponse colaboradorResponse = response.body();
                    String fileUrl = colaboradorResponse.getData().getFile();
                    Integer code = colaboradorResponse.getCode();

                    boolean zipExists = Util.checkFileExist(getActivity(), "employees_data.json.zip");
                    if (code == 200 && zipExists) {
                        Log.v(TAG, "fetchColaboradores: ¿Zip ? " + zipExists);
                        boolean jsonExists = Util.checkFileExist(getActivity(), "employees_data.json");
                        if (!jsonExists)
                            Log.v(TAG, "fetchColaboradores: ¿Zip descomprimido? " + Util.unpackZipFile(getActivity(), "employees_data.json.zip"));
                        else
                            Log.e(TAG, "fetchColaboradores: No existe employees_data.json");
                    } else if (code == 200) {
                        fetchColaboradoresFile(fileUrl);
                    } else {
                        Log.e(TAG, "fetchColaboradores: Hubo un problema con el archivo, código: " + code
                                + " archivo existe: " + zipExists);
                    }
                } else {
                    int statusCode = response.code();
                    Log.e(TAG, "onResponse(): Código de error = " + statusCode);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ColaboradorResponse> call, @NotNull Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchColaboradoresFile(String fileUrl) {
        Call<ResponseBody> call = colaboradorService.donwloadFileWithURL(fileUrl);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "fetchColaboradoresFile: Hubo respueta del servidor y sí hay un archivo");
                    boolean writtenToDisk = Util.writeResponseBodyToDisk(getActivity(), response.body());
                    Log.d(TAG, "fetchColaboradoresFile: ¿Se descargo el archivo de forma correcta? " + writtenToDisk);
                } else {
                    Log.d(TAG, "fetchColaboradoresFile: Comunicación con el servidor fallo");
                }
            }
            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                Log.e(TAG, "error");
            }
        });
    }

    private Colaboradores getColaboradoresFromJson() {
        String json = null;
        Colaboradores colaboradores = null;
        try {
            json = Util.readJsonFile(getActivity(), "employees_data.json");
            Log.v(TAG, "getColaboradoresFromJson: " + json);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        Gson gson = new Gson();
        colaboradores = gson.fromJson(json, Colaboradores.class);

        return colaboradores;
    }

}
