package com.ranferi.android.examenandroidneta.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.firebase.firestore.FirebaseFirestore;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.model.Colaborador;
import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradorFragment;

import java.util.List;

public class ColaboradorPagerActivity extends FragmentActivity {
    private static final String COLABORADOR_ID = "com.ranferi.android.examenandroidneta.colaborador_id";

    private ViewPager2 mViewPager;
    private List<Colaborador> mColaboradorList;
    private ColaboradorDB mDb;
    private int colaboradorId;

    public static Intent newIntent(Context context, int id) {
        Intent intent = new Intent(context, ColaboradorPagerActivity.class);
        intent.putExtra(COLABORADOR_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colaborador_pager);

        colaboradorId = (int) getIntent().getSerializableExtra(COLABORADOR_ID);

        mViewPager = findViewById(R.id.colaborador_view_pager);

        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        mDb = new ColaboradorDB(firestore);

        getColaboradores();
    }

    public void getColaboradores() {
        mDb.getAllDocs("colaboradores", new ColaboradorDB.AllDocsCallback() {
            @Override
            public void allDocsResult(List<Colaborador> colaboradorList) {
                mColaboradorList = colaboradorList;
                updateUI();
            }
        });
    }

    private void updateUI() {
        mViewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                Colaborador colaborador = mColaboradorList.get(position);
                return ColaboradorFragment.newInstance(colaborador.getId());
            }

            @Override
            public int getItemCount() {
                return mColaboradorList.size();
            }
        });

        for (int i = 0; i < mColaboradorList.size(); i++) {
            if (mColaboradorList.get(i).getId().equals(colaboradorId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
