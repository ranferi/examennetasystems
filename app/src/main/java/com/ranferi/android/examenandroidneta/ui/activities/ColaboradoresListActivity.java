package com.ranferi.android.examenandroidneta.ui.activities;

import androidx.fragment.app.Fragment;

import com.ranferi.android.examenandroidneta.ui.fragments.ColaboradoresListFragment;
import com.ranferi.android.examenandroidneta.ui.base.SingleFragmentActivity;

public class ColaboradoresListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new ColaboradoresListFragment();
    }
}