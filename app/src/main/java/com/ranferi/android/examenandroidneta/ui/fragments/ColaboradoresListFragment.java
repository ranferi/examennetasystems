package com.ranferi.android.examenandroidneta.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ranferi.android.examenandroidneta.db.ColaboradorDB;
import com.ranferi.android.examenandroidneta.ui.activities.ColaboradoresMapActivity;
import com.ranferi.android.examenandroidneta.R;
import com.ranferi.android.examenandroidneta.ui.adapter.ColaboradorAdapter;
import com.ranferi.android.examenandroidneta.model.Colaborador;

import java.util.List;

public class ColaboradoresListFragment extends Fragment {

    private RecyclerView mColaboradoresRecyclerView;
    private ColaboradorAdapter mAdapter;
    private ColaboradorDB mColaboradorDB;
    private List<Colaborador> mColaboradorList;
    private Button mColaboradoresMap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colaboradores_list, container, false);

        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        mColaboradorDB = new ColaboradorDB(firestore);

        mColaboradoresMap = view.findViewById(R.id.button_mapa_colaboradores);
        mColaboradoresMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ColaboradoresMapActivity.class));
            }
        });
        mColaboradoresRecyclerView = view.findViewById(R.id.recycler_colaboradores_list);
        mColaboradoresRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return view;
    }

    private void updateUI() {
        mColaboradorDB.getAllDocs("colaboradores", new ColaboradorDB.AllDocsCallback() {
            @Override
            public void allDocsResult(List<Colaborador> colaboradorList) {
                mColaboradorList = colaboradorList;
                mAdapter = new ColaboradorAdapter(mColaboradorList, getActivity());
                mColaboradoresRecyclerView.setAdapter(mAdapter);
            }
        });
    }
}
